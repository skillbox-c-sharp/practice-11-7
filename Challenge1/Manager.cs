﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Challenge1
{
    public class Manager : Consultant
    {
        public Manager(string name) : base(name) { }

        public void AddClient(Client client, List<Client> clientsList)
        {
            clientsList.Add(client);
        }

        public void RemoveClient(int index, List<Client> clientsList)
        {
            clientsList.RemoveAt(index);
        }

        public void ChangeFirstName(Client client, string firstName)
        {
            client.ChangeFirstName(firstName, true);
        }

        public void ChangeMiddleName(Client client, string middleName)
        {
            client.ChangeMiddleName(middleName, true);
        }

        public void ChangeLastName(Client client, string lastName)
        {
            client.ChangeLastName(lastName, true);
        }

        public void ChangeSerialOfDocument(Client client, string serialOfDocument)
        {
            client.ChangeSerialOfDocument(serialOfDocument, true);
        }

        public override string PrintClient(Client client)
        {
            return client.GetClientInfo(true);
        }

        public override void ChangeTelephoneNumber(Client client, string value)
        {
            client.ChangePhoneNumber(value, true);
        }
    }
}
