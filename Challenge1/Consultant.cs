﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Numerics;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Challenge1
{
    public class Consultant : IEmployee
    {
        public string Name { get; set; }

        public Consultant(string name)
        {
            Name = name;
        }

        public virtual string PrintClient(Client client)
        {
            return client.GetClientInfo(false);
        }
        public virtual void ChangeTelephoneNumber(Client client, string value)
        {
           client.ChangePhoneNumber(value, false);
        }
    }
}
