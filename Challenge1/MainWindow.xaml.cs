﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Linq;

namespace Challenge1
{
    public partial class MainWindow : Window
    {
        Consultant consultant;
        Manager manager;
        bool flagManager;
        List<Client> clientsList;
        List<Client> allClientsList;
        bool isSorted;
        public MainWindow()
        {
            InitializeComponent();
            allClientsList = SetAllClientsList();
            clientsList = new List<Client>();
            SetClientsList();
            consultant = new Consultant("Консультант");
            manager = new Manager("Менеджер");
            flagManager = false;
            isSorted = false;
            departmentsComboBox.SelectedIndex = 0;
            SetDepartmentsComboBox();
            HideAllElements();
        }

        private void employeeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            clientsListView.Items.Clear();
            string element = ((Label)employeeComboBox.SelectedItem).Content.ToString()!;
            if (element == "Консультант")
            {
                flagManager = false;
                ShowForConsultant();
            }
            else
            {
                ShowAllElements();
            }
            RefreshClientsListView();
        }

        static List<Client> SetAllClientsList()
        {
            List<Client> clients = new List<Client>();

            XDocument xdoc = XDocument.Load("clients.xml");
            foreach (XElement element in xdoc.Element("Clients").Elements("Client"))
            {
                XAttribute firstNameAttribute = element.Attribute("FirstName");
                XAttribute middleNameAttribute = element.Attribute("MiddleName");
                XAttribute lastNameAttribute = element.Attribute("LastName");
                XElement serialElement = element.Element("SerialOfDocument");
                XElement phoneElement = element.Element("MobilePhone");
                XElement ChangeTimeElement = element.Element("ChangeTime");
                XElement changeLogElement = element.Element("ChangeLog");
                XElement whoChangeElement = element.Element("WhoChange");
                XElement departmentIdElement = element.Element("DepartmentId");

                clients.Add(new Client(firstNameAttribute.Value, middleNameAttribute.Value, 
                    lastNameAttribute.Value, long.Parse(phoneElement.Value), 
                    serialElement.Value, DateTime.Parse(ChangeTimeElement.Value), 
                    changeLogElement.Value, whoChangeElement.Value, Int32.Parse(departmentIdElement.Value)));
            }
            return clients;
        }
        private void SetClientsList()
        {
            allClientsList.AddRange(clientsList);
            clientsList.Clear();
            IEnumerable<Client> query = allClientsList.Where(c => c.DepartmentId == departmentsComboBox.SelectedIndex);
            foreach (Client client in query)
            {
                clientsList.Add(client);
            }
            allClientsList.RemoveAll(c => c.DepartmentId == departmentsComboBox.SelectedIndex);
        }

        private void changeNumberButton_Click(object sender, RoutedEventArgs e)
        {
            if (clientsListView.SelectedIndex != -1 && !changedTextBox.Text.Equals(""))
            {
                string value = changedTextBox.Text;
                bool success = Int64.TryParse(value, out long number);
                if (success && value.Length >= 10)
                {
                    if (flagManager)
                    {
                        manager.ChangeTelephoneNumber(clientsList[clientsListView.SelectedIndex], value);
                    }
                    else
                    {
                        consultant.ChangeTelephoneNumber(clientsList[clientsListView.SelectedIndex], value);
                    }
                    RefreshClientsListView();
                }
                else
                {
                    MessageBox.Show("Номер телефона введен неверно", "", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
            else
            {
                MessageBox.Show("Необходимо заполнить поле для изменения выше и выбрать клиента", "", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void changeFirstNameButton_Click(object sender, RoutedEventArgs e)
        {
            if (clientsListView.SelectedIndex != -1 && !changedTextBox.Text.Equals(""))
            {
                if (flagManager)
                {
                    manager.ChangeFirstName(clientsList[clientsListView.SelectedIndex], changedTextBox.Text);
                }
                RefreshClientsListView();
            }
            else
            {
                MessageBox.Show("Необходимо заполнить поле для изменения выше и выбрать клиента", "", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void changeLastNameButton_Click(object sender, RoutedEventArgs e)
        {
            if (clientsListView.SelectedIndex != -1 && !changedTextBox.Text.Equals(""))
            {
                if (flagManager)
                {
                    manager.ChangeLastName(clientsList[clientsListView.SelectedIndex], changedTextBox.Text);
                }
                RefreshClientsListView();
            }
            else
            {
                MessageBox.Show("Необходимо заполнить поле для изменения выше и выбрать клиента", "", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void changeMiddleNameButton_Click(object sender, RoutedEventArgs e)
        {
            if (clientsListView.SelectedIndex != -1 && !changedTextBox.Text.Equals(""))
            {
                if (flagManager)
                {
                    manager.ChangeMiddleName(clientsList[clientsListView.SelectedIndex], changedTextBox.Text);
                }
                RefreshClientsListView();
            }
            else
            {
                MessageBox.Show("Необходимо заполнить поле для изменения выше и выбрать клиента", "", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

        }

        private void changePassporButton_Click(object sender, RoutedEventArgs e)
        {
            if (clientsListView.SelectedIndex != -1 && !changedTextBox.Text.Equals(""))
            {
                if (flagManager)
                {
                    manager.ChangeSerialOfDocument(clientsList[clientsListView.SelectedIndex], changedTextBox.Text);
                }
                RefreshClientsListView();
            }
            else
            {
                MessageBox.Show("Необходимо заполнить поле для изменения выше и выбрать клиента", "", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void addClientButton_Click(object sender, RoutedEventArgs e)
        {
            string tempFirstName = firstNameTextBox.Text;
            string tempLastName = lastNameTextBox.Text;
            string tempMiddleName = middleNameTextBox.Text;
            string tempPhone = phoneTextBox.Text;
            string tempPassport = passportTextBox.Text;
            int tempDepartmentId = departmentsComboBox.SelectedIndex;
            if (!(tempFirstName.Equals("") && tempLastName.Equals("") && tempMiddleName.Equals("") && tempPhone.Equals("") && tempPassport.Equals(""))) 
            {
                bool success = Int64.TryParse(tempPhone, out long number);
                if (success && tempPhone.Length >= 10) 
                {
                    Client tempClient = new Client(tempFirstName, tempMiddleName, tempLastName, long.Parse(tempPhone), tempPassport, tempDepartmentId);
                    if (flagManager)
                    {
                        manager.AddClient(tempClient, clientsList);
                    }
                    RefreshClientsListView();
                }
                else
                {
                    MessageBox.Show("Номер телефона введен неверно", "", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
            else
            {
                MessageBox.Show("Необходимо заполнить все поля для добавления клиента", "", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private void RefreshClientsListView()
        {
            if (employeeComboBox.SelectedIndex != -1)
            {
                SetClientsList();
                clientsListView.Items.Clear();
                for (int i = 0; i < clientsList.Count; i++)
                {
                    if (flagManager)
                    {
                        string[] tempData = manager.PrintClient(clientsList[i]).Split("##");
                        string newData = String.Join(" ", tempData);
                        clientsListView.Items.Add(newData);
                    }
                    else
                    {
                        string[] tempData = consultant.PrintClient(clientsList[i]).Split("##");
                        string newData = String.Join(" ", tempData);
                        clientsListView.Items.Add(newData);
                    }
                }
            }
        }

        void GridViewColumnHeaderClickedHandler(object sender, RoutedEventArgs e)
        {
            if (!isSorted)
            {
                clientsList.Sort();
                isSorted = true;
            }
            else
            {
                clientsList.Reverse();
            }
            RefreshClientsListView();
        }

        private void HideAllElements()
        {
            changedTextBox.Visibility = Visibility.Hidden;
            changeDataLabel.Visibility = Visibility.Hidden;
            changeNumberButton.Visibility = Visibility.Hidden;
            changeFirstNameButton.Visibility = Visibility.Hidden;
            changeLastNameButton.Visibility = Visibility.Hidden;
            changeMiddleNameButton.Visibility = Visibility.Hidden;
            changePassporButton.Visibility = Visibility.Hidden;
            addClientButton.Visibility = Visibility.Hidden;
            firstNameLabel.Visibility = Visibility.Hidden;
            middleNameLabel.Visibility = Visibility.Hidden;
            lastNameLabel.Visibility = Visibility.Hidden;
            phoneLabel.Visibility = Visibility.Hidden;
            passportLabel.Visibility = Visibility.Hidden;
            firstNameTextBox.Visibility = Visibility.Hidden;
            middleNameTextBox.Visibility = Visibility.Hidden;
            lastNameTextBox.Visibility = Visibility.Hidden;
            phoneTextBox.Visibility = Visibility.Hidden;
            passportTextBox.Visibility = Visibility.Hidden;
            saveButton.Visibility = Visibility.Hidden;
            departmentsComboBox.Visibility = Visibility.Hidden;
            departmentLabel.Visibility = Visibility.Hidden;
            removeClientButton.Visibility = Visibility.Hidden;
        }

        private void ShowAllElements()
        {
            flagManager = true;
            changedTextBox.Visibility = Visibility.Visible;
            changeDataLabel.Visibility = Visibility.Visible;
            changeNumberButton.Visibility = Visibility.Visible;
            changeFirstNameButton.Visibility = Visibility.Visible;
            changeLastNameButton.Visibility = Visibility.Visible;
            changeMiddleNameButton.Visibility = Visibility.Visible;
            changePassporButton.Visibility = Visibility.Visible;
            addClientButton.Visibility = Visibility.Visible;
            firstNameLabel.Visibility = Visibility.Visible;
            middleNameLabel.Visibility = Visibility.Visible;
            lastNameLabel.Visibility = Visibility.Visible;
            phoneLabel.Visibility = Visibility.Visible;
            passportLabel.Visibility = Visibility.Visible;
            firstNameTextBox.Visibility = Visibility.Visible;
            middleNameTextBox.Visibility = Visibility.Visible;
            lastNameTextBox.Visibility = Visibility.Visible;
            phoneTextBox.Visibility = Visibility.Visible;
            passportTextBox.Visibility = Visibility.Visible;
            saveButton.Visibility = Visibility.Visible;
            departmentsComboBox.Visibility = Visibility.Visible;
            departmentLabel.Visibility = Visibility.Visible;
            removeClientButton.Visibility = Visibility.Visible;
        }

        private void ShowForConsultant()
        {
            changedTextBox.Visibility = Visibility.Visible;
            changeDataLabel.Visibility = Visibility.Visible;
            changeNumberButton.Visibility = Visibility.Visible;
            changeFirstNameButton.Visibility = Visibility.Hidden;
            changeLastNameButton.Visibility = Visibility.Hidden;
            changeMiddleNameButton.Visibility = Visibility.Hidden;
            changePassporButton.Visibility = Visibility.Hidden;
            addClientButton.Visibility = Visibility.Hidden;
            firstNameLabel.Visibility = Visibility.Hidden;
            middleNameLabel.Visibility = Visibility.Hidden;
            lastNameLabel.Visibility = Visibility.Hidden;
            phoneLabel.Visibility = Visibility.Hidden;
            passportLabel.Visibility = Visibility.Hidden;
            firstNameTextBox.Visibility = Visibility.Hidden;
            middleNameTextBox.Visibility = Visibility.Hidden;
            lastNameTextBox.Visibility = Visibility.Hidden;
            phoneTextBox.Visibility = Visibility.Hidden;
            passportTextBox.Visibility = Visibility.Hidden;
            saveButton.Visibility = Visibility.Hidden;
            departmentsComboBox.Visibility = Visibility.Visible;
            departmentLabel.Visibility = Visibility.Visible;
            removeClientButton.Visibility = Visibility.Hidden;
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            List<Client> tempAllClientsList = new List<Client>();
            tempAllClientsList.AddRange(allClientsList);
            tempAllClientsList.AddRange(clientsList);
            XElement xClients = new XElement("Clients");
            for (int i = 0; i < tempAllClientsList.Count; i++)
            {
                if (flagManager)
                {
                    string[] tempData = manager.PrintClient(tempAllClientsList[i]).Split("##");
                    XElement clientElement = new XElement("Client",
                        new XAttribute($"FirstName", tempData[0]),
                        new XAttribute($"MiddleName", tempData[1]),
                        new XAttribute($"LastName", tempData[2]),
                        new XElement("SerialOfDocument", tempData[3]),
                        new XElement("MobilePhone", tempData[4]),
                        new XElement("ChangeTime", tempData[5]),
                        new XElement("ChangeLog", tempData[6]),
                        new XElement("WhoChange", tempData[7]),
                        new XElement("DepartmentId", tempData[8]));
                    xClients.Add(clientElement);
                }
            }
            xClients.Save("clients.xml");
        }

        private void removeClientButton_Click(object sender, RoutedEventArgs e)
        {
            if (clientsListView.SelectedIndex != -1)
            {
                if (flagManager)
                {
                    manager.RemoveClient(clientsListView.SelectedIndex, clientsList);
                }
                RefreshClientsListView();
            }
        }

        private void SetDepartmentsComboBox()
        {
            for (int i = 0; i < Client.GetDepartmentsCount(); i++)
            {
                departmentsComboBox.Items.Add(Client.GetDepartmentName(i));
            }
        }

        private void departmentsComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            RefreshClientsListView();
        }
    }
}
