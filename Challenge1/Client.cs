﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Challenge1
{
    public class Client : IComparable<Client>
    {
        private long telephoneNumber;
        private string serialOfDocument;
        private string firstName;
        private string middleName;
        private string lastName;
        private DateTime changeDate;
        private string changeLog;
        private string whoChange;
        private int departmentId;

        static int departmentsCount;

        static Client()
        {
            departmentsCount = 3;
        }

        public static int GetDepartmentsCount()
        {
            return departmentsCount;
        }

        public static string GetDepartmentName(int index)
        {
            switch (index)
            {
                case 0:
                    return "Кредиты";
                case 1:
                    return "Ипотеки";
                case 2:
                    return "Вклады";
                default:
                    return "";
            }
        }

        private string FirstName { get { return firstName; } set { firstName = value; } }
        private string MiddleName { get { return middleName; } set { middleName = value; } }
        private string LastName { get { return lastName; } set { lastName = value; } }
        private DateTime ChangeDate { get { return changeDate; } set { changeDate = value; } }
        private string ChangeLog { get { return changeLog; } set { changeLog = value; } }
        private string WhoChange { get { return whoChange; } set { whoChange = value; } }
        private string SerialOfDocument { get { return serialOfDocument; } set { serialOfDocument = value; } }
        private long TelephoneNumber
        {
            get
            {
                return telephoneNumber;
            }
            set
            {
                if (value.ToString().Length >= 10) telephoneNumber = value;
                else telephoneNumber = 1234567890;
            }
        }
        public int DepartmentId { get { return departmentId; } }

        public Client(string firstName, string middleName, string lastName, long telephoneNumber, string serialOfDocument, int departmentId)
        {
            FirstName = firstName;
            MiddleName = middleName;
            LastName = lastName;
            TelephoneNumber = telephoneNumber;
            SerialOfDocument = serialOfDocument;
            ChangeDate = DateTime.Now;
            ChangeLog = "Новый клиент";
            WhoChange = "Банк";
            this.departmentId = departmentId;
        }

        public Client(string firstName, string middleName, string lastName, long telephoneNumber, string serialOfDocument, DateTime dateTime, string changeLog, string whoChange, int departmentId) 
            : this(firstName, middleName, lastName, telephoneNumber, serialOfDocument, departmentId)
        {
            ChangeDate = dateTime;
            ChangeLog = changeLog;
            WhoChange = whoChange;
        }

        public string GetClientInfo(bool flagManager)
        {
            if (flagManager) return this.FirstName + "##" + this.MiddleName + "##" + this.LastName +
                "##" + this.SerialOfDocument + "##" + this.TelephoneNumber + "##" + this.ChangeDate + "##" +
                this.ChangeLog + "##" + this.WhoChange + "##" + this.DepartmentId;
            else return this.FirstName + "##" + this.MiddleName + "##" + this.LastName +
                "##" + "******************" + "##" + this.TelephoneNumber + "##" + this.ChangeDate + "##" +
                this.ChangeLog + "##" + this.WhoChange + "##" + this.DepartmentId;
        }

        public void ChangePhoneNumber(string value, bool flagManager)
        {
            bool success = Int64.TryParse(value, out long number);
            if (success && value.ToString().Length >= 10)
            {
                this.TelephoneNumber = number;
                this.ChangeDate = DateTime.Now;
                this.ChangeLog = "Изменен номер телефона";
                this.WhoChange = flagManager ? "Менедженер" : "Консультант";
            }

        }
        public void ChangeFirstName(string value, bool flagManager)
        {
            if (flagManager)
            {
                this.FirstName = value;
                this.ChangeDate = DateTime.Now;
                this.ChangeLog = "Изменено имя";
                this.WhoChange = "Менедженер";
            }
        }

        public void ChangeMiddleName(string value, bool flagManager)
        {
            if (flagManager)
            {
                this.MiddleName = value;
                this.ChangeDate = DateTime.Now;
                this.ChangeLog = "Изменено отчество";
                this.WhoChange = "Менедженер";
            }
        }

        public void ChangeLastName(string value, bool flagManager)
        {
            if (flagManager)
            {
                this.LastName = value;
                this.ChangeDate = DateTime.Now;
                this.ChangeLog = "Изменена фамилия";
                this.WhoChange = "Менедженер";
            }
        }

        public void ChangeSerialOfDocument(string value, bool flagManager)
        {
            if (flagManager)
            {
                this.SerialOfDocument = value;
                this.ChangeDate = DateTime.Now;
                this.ChangeLog = "Изменен паспорт";
                this.WhoChange = "Менедженер";
            }
        }

        public int CompareTo(Client? other)
        {
            return String.Compare(this.FirstName, other!.FirstName);
        }

    }
}
